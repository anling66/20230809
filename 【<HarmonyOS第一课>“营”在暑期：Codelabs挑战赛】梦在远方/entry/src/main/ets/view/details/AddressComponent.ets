import { AppFontSize, AppFontWeight, LAYOUT_WIDTH_OR_HEIGHT, AddressPicker } from '../../common/CommonConstants';

@Component
export default struct AddressComponent {
  build() {
    Row() {
      Column() {
        Text($r('app.string.detail_address_text'))
          .maxLines(AddressPicker.MAX_LINES)
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .textCase(TextCase.UpperCase)
          .fontSize(AppFontSize.SMALL)
          .fontColor($r('app.color.text'))
          .fontWeight(AppFontWeight.BOLDER)
      }
      .width(AddressPicker.LAYOUT_WEIGHT_LEFT)
      .alignItems(HorizontalAlign.Start)

      Row() {
        Image($rawfile('detail/detail_location.png'))
          .objectFit(ImageFit.Contain)
          .height(AddressPicker.IMAGE_SIZE_LOCATION)
          .width(AddressPicker.IMAGE_SIZE_LOCATION)
          .margin({ right: AddressPicker.MARGIN_RIGHT_IMAGE })
        Text($r('app.string.detail_address_holder_text'))
          .maxLines(AddressPicker.MAX_LINES)
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .textCase(TextCase.UpperCase)
          .fontSize(AppFontSize.SMALL)
          .fontColor(Color.Black)
          .fontWeight(AppFontWeight.BOLD)
      }
      .justifyContent(FlexAlign.Start)
      .width(AddressPicker.LAYOUT_WEIGHT_CENTER)

      Row() {
        Image($rawfile('detail/detail_pick_up_more.png'))
          .objectFit(ImageFit.Contain)
          .height(AddressPicker.IMAGE_SIZE_MORE)
          .width(AddressPicker.IMAGE_SIZE_MORE)
      }
      .width(AddressPicker.LAYOUT_WEIGHT_RIGHT)
      .justifyContent(FlexAlign.End)
    }
    .width(LAYOUT_WIDTH_OR_HEIGHT)
  }
}